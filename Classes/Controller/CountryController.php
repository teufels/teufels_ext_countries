<?php
namespace TEUFELS\TeufelsExtCountries\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * CountryController
 */
class CountryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * countryRepository
     *
     * @var \TEUFELS\TeufelsExtCountries\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository = NULL;
    
    /**
     * action checkboxList
     *
     * @return void
     */
    public function checkboxListAction()
    {
        /*
         * get content element uid
         */
        
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];
        $sRequestUri = $_SERVER['REQUEST_URI'];
        $this->view->assign('iPluginUid', $iPluginUid);
        $aSelectedCountries = array();
        if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('tx_teufelsextcountries_teufelsextcountriescheckboxlist') != NULL) {
            $tx_teufelsextcountries_teufelsextcountriescheckboxlist_arguments = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('tx_teufelsextcountries_teufelsextcountriescheckboxlist');
            $aSelectedCountries = $tx_teufelsextcountries_teufelsextcountriescheckboxlist_arguments['selectedCountries'];
        }
        $aCountries = $this->countryRepository->findAll()->toArray();
        $aCountriesSelectState = array();
        $sIn = '';
        foreach ($aCountries as $oCountry) {
            if (is_array($aSelectedCountries) && count($aSelectedCountries) > 0) {
                if (in_array($oCountry->getUid(), $aSelectedCountries)) {
                    $aCountriesSelectState[$oCountry->getUid()] = array(
                        $oCountry,
                        1
                    );
                    $sIn = 'in';
                } else {
                    $aCountriesSelectState[$oCountry->getUid()] = array(
                        $oCountry,
                        0
                    );
                }
            } else {
                $aCountriesSelectState[$oCountry->getUid()] = array(
                    $oCountry,
                    0
                );
            }
        }
        $this->view->assign('sIn', $sIn);
        $this->view->assign('aCountries', $aCountries);
        $this->view->assign('aCountriesSelectState', $aCountriesSelectState);
        $this->view->assign('iPluginUid', $iPluginUid);
        $this->view->assign('sRequestUri', $sRequestUri);
    }
    
    /**
     * action selectList
     *
     * @return void
     */
    public function selectListAction()
    {
        /*
         * get content element uid
         */

        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];
        $sRequestUri = $_SERVER['REQUEST_URI'];
        $this->view->assign('iPluginUid', $iPluginUid);
        $aSelectedCountries = array();
        if (\TYPO3\CMS\Core\Utility\GeneralUtility::_GP('tx_teufelsextcountries_teufelsextcountriesselectlist') != NULL) {
            $tx_teufelsextcountries_teufelsextcountriesselectlist_arguments = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('tx_teufelsextcountries_teufelsextcountriesselectlist');
            $sSelectedCountry = $tx_teufelsextcountries_teufelsextcountriesselectlist_arguments['selectedCountry'];
        }
        $aCountries = $this->countryRepository->findAll()->toArray();
        $aCountriesSelectState = array();
        $sSelected = '';
        foreach ($aCountries as $oCountry) {
            if (isset($sSelectedCountry) && $sSelectedCountry != "") {
                if ($oCountry->getUid() == $sSelectedCountry) {
                    $aCountriesSelectState[$oCountry->getUid()] = array(
                        $oCountry,
                        'selected'
                    );
                } else {
                    $aCountriesSelectState[$oCountry->getUid()] = array(
                        $oCountry,
                        ''
                    );
                }
            } else {
                $aCountriesSelectState[$oCountry->getUid()] = array(
                    $oCountry,
                    ''
                );
            }
        }
        $this->view->assign('aCountries', $aCountries);
        $this->view->assign('aCountriesSelectState', $aCountriesSelectState);
        $this->view->assign('iPluginUid', $iPluginUid);
        $this->view->assign('sRequestUri', $sRequestUri);
    }

}