<?php
namespace TEUFELS\TeufelsExtCountries\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Country
 */
class Country extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';
    
    /**
     * backendTitle
     *
     * @var string
     * @validate NotEmpty
     */
    protected $backendTitle = '';
    
    /**
     * belongsToDefaultSysLanguage
     *
     * @var bool
     * @validate NotEmpty
     */
    protected $belongsToDefaultSysLanguage = false;
    
    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Returns the backendTitle
     *
     * @return string $backendTitle
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }
    
    /**
     * Sets the backendTitle
     *
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }
    
    /**
     * Returns the belongsToDefaultSysLanguage
     *
     * @return bool $belongsToDefaultSysLanguage
     */
    public function getBelongsToDefaultSysLanguage()
    {
        return $this->belongsToDefaultSysLanguage;
    }
    
    /**
     * Sets the belongsToDefaultSysLanguage
     *
     * @param bool $belongsToDefaultSysLanguage
     * @return void
     */
    public function setBelongsToDefaultSysLanguage($belongsToDefaultSysLanguage)
    {
        $this->belongsToDefaultSysLanguage = $belongsToDefaultSysLanguage;
    }
    
    /**
     * Returns the boolean state of belongsToDefaultSysLanguage
     *
     * @return bool
     */
    public function isBelongsToDefaultSysLanguage()
    {
        return $this->belongsToDefaultSysLanguage;
    }

}