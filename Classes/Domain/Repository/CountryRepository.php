<?php
namespace TEUFELS\TeufelsExtCountries\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Countries
 */
class CountryRepository extends \TEUFELS\TeufelsExtCountries\Domain\Repository\AbstractRepository
{

    protected $defaultOrderings = array(
        'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

    /**
     * @param int $iUid
     * @param $bDebug
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUid($iUid, $bDebug = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->matching($query->in('uid', array($iUid)));
        $this->debugQuery($query, $bDebug);
        return $query->execute();
    }
    
    /**
     * @param $iSysLanguageList $aSysLanguageList
     * @param $sDebug
     * @return array
     */
    public function getCountriesBySysLanguages($aSysLanguageList = array(), $sDebug = false)
    {
        /*
         * SELECT
         */
        
        $aSelect = array(
            'tx_teufelsextcountries_domain_model_country.uid'
        );
        $sSelect = implode(',', $aSelect);
        /*
         * FROM
         */
        
        $aFrom = array(
            'tx_teufelsextcountries_domain_model_country',
            'tx_teufelsextcountries_domain_model_country_sys_language_mm'
        );
        $sFrom = implode(',', $aFrom);
        /*
         * WHERE
         */
        
        $aWhere = array(
            'tx_teufelsextcountries_domain_model_country.sys_language_uid IN (-1,0)',
            'tx_teufelsextcountries_domain_model_country.uid = tx_teufelsextcountries_domain_model_country_sys_language_mm.uid_local'
        );
        $aWhereOr = array();
        $sSysLanguageList = implode(',', $aSysLanguageList);
        $aWhereOr[] = "tx_teufelsextcountries_domain_model_country_sys_language_mm.uid_foreign IN ({$sSysLanguageList})";
        if (in_array(0, $aSysLanguageList)) {
            $aWhereOr[] = 'tx_teufelsextcountries_domain_model_country.belongs_to_default_sys_language = 1';
        }
        $aWhere[] = ' ( ' . implode(' OR ', $aWhereOr) . ' ) ';
        $sWhere = implode(' AND ', $aWhere);
        \TYPO3\CMS\Core\Utility\DebugUtility::debug($aWhere, 'query');
        \TYPO3\CMS\Core\Utility\DebugUtility::debug($aWhereOr, 'query');
        /*
         * GROUP BY
         */
        
        $sGroupBy = '';
        /*
         * ORDER BY
         */
        
        $sOrderBy = '';
        /*
         * LIMIT (AND OFFSET)
         */
        
        $sLimitOffest = '';
        $aResult = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($sSelect, $sFrom, $sWhere, $sGroupBy, $sOrderBy, $sLimitOffest);
        return $aResult;
    }

}