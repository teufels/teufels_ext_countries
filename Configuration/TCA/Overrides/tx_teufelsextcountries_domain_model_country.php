<?php

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$sModel = 'tx_teufelsextcountries_domain_model_country';

$GLOBALS['TCA'][$sModel]['columns']['sys_language_mm'] = [
    'exclude' => 0,
    'label' => 'LLL:EXT:teufels_ext_countries/Resources/Private/Language/translation_db.xlf:sysfilemetadata.sys_language_mm',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectMultipleSideBySide',
        'foreign_table' => 'sys_language',
        'MM' => 'tx_teufelsextcountries_domain_model_country_sys_language_mm',
        'size' => 10,
        'autoSizeMax' => 30,
        'maxitems' => 9999,
        'multiple' => 0,
        'enableMultiSelectFilterTextfield' => TRUE,
        'wizards' => [
            '_PADDING' => 1,
            '_VERTICAL' => 1,
        ],
    ],
];

$GLOBALS['TCA'][$sModel]['types']['1']['showitem'] = 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, backend_title, belongs_to_default_sys_language, sys_language_mm, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime';