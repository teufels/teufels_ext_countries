<?php

namespace TEUFELS\TeufelsExtCountries\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \TEUFELS\TeufelsExtCountries\Domain\Model\Country.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CountryTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \TEUFELS\TeufelsExtCountries\Domain\Model\Country
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \TEUFELS\TeufelsExtCountries\Domain\Model\Country();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getBackendTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getBackendTitle()
		);
	}

	/**
	 * @test
	 */
	public function setBackendTitleForStringSetsBackendTitle()
	{
		$this->subject->setBackendTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'backendTitle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getBelongsToDefaultSysLanguageReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getBelongsToDefaultSysLanguage()
		);
	}

	/**
	 * @test
	 */
	public function setBelongsToDefaultSysLanguageForBoolSetsBelongsToDefaultSysLanguage()
	{
		$this->subject->setBelongsToDefaultSysLanguage(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'belongsToDefaultSysLanguage',
			$this->subject
		);
	}
}
