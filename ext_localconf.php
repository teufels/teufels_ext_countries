<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextcountriescheckboxlist',
	array(
		'Country' => 'checkboxList',
		
	),
	// non-cacheable actions
	array(
		'Country' => 'checkboxList',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextcountriesselectlist',
	array(
		'Country' => 'selectList',
		
	),
	// non-cacheable actions
	array(
		'Country' => 'selectList',
		
	)
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder