<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextcountriescheckboxlist',
	'teufels_ext_countries :: checkbox list'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextcountriesselectlist',
	'teufels_ext_countries :: select list'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'teufels_ext_countries');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextcountries_domain_model_country', 'EXT:teufels_ext_countries/Resources/Private/Language/locallang_csh_tx_teufelsextcountries_domain_model_country.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextcountries_domain_model_country');
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder